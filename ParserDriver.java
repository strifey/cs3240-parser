import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class ParserDriver {
	static GrammarParser parserGenerator;
	static HashMap<Nonterminal, HashMap<Terminal, Rule>> parsingTable;
	static Lexer lexer;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			parserGenerator = new GrammarParser(args[0]);
			lexer = new Lexer(new BufferedReader(new FileReader(args[1])));
			parsingTable = parserGenerator.parsingTable.mnt;
			Nonterminal startSymbol = parserGenerator.getStartToken();

			System.out.println(parse(lexer, parsingTable, startSymbol));
			FileWriter fw = new FileWriter(new File("parsingTableOut.txt"));
			fw.write(parserGenerator.parsingTable.toString());
			fw.close();

		} catch (FileNotFoundException e) {
			System.out
					.println("Correct command-line procedure call is as follows:\n"
							+ "\t java ParserDriver grammarrules programparsed");
		} catch (IOException e) {
			System.out
					.println("Correct command-line procedure call is as follows:\n"
							+ "\t java ParserDriver grammarrules programparsed");
		}
	}

	public static String parse(Lexer lexer,
			HashMap<Nonterminal, HashMap<Terminal, Rule>> parsingTable,
			Nonterminal startSymbol) {

		Stack<Token> stack = new Stack<Token>();
		ArrayList<Terminal> inputs = lexer.getTokens();
		int count = 0;

		stack.push(startSymbol);
		while (!stack.isEmpty() && count < inputs.size()) {
			Token top = stack.peek();
			System.out.println("Testing top: "+ top.toString()+", next input: "+inputs.get(count).toString());
			if (top instanceof Nonterminal) {

				Nonterminal nextNonTerm = (Nonterminal) top;
				Rule rules = parsingTable.get(nextNonTerm).get(
						inputs.get(count));
				if (rules == null)
					return "Invalid. No rule found for "
							+ nextNonTerm.toString() + " and the input "
							+ inputs.get(count);

				stack.pop();

				for (int i = rules.rule.length - 1; i > -1; i--){
					if(!rules.rule[i].equals(new Terminal(Terminal.TerminalType.EPSILON)))
						stack.push(rules.rule[i]);
				}

			} else if (top instanceof Terminal) {

				Terminal nextTerm = (Terminal) top;
				Terminal nextInput = inputs.get(count);

				if (nextTerm.equals(nextInput)) {
					++count;
					stack.pop();

				} else
					return nextTerm + " and " + nextInput
							+ " do not match: invalid";
			}
			System.out.println("Current stack: "+ stack.toString());
		}

		if (stack.isEmpty())
			return "Valid";
		return "Invalid. Ran out of tokens early or ran over input count";
	}
}
