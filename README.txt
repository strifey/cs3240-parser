COMPILING
_________

1) From the command-line, run "javac ParserDriver.java" from the directory where all the code is located

RUNNING
_______

1) From run java ParserDriver <grammar> <program>, where <grammar> is the grammar file and <program> contains the input that will be parsed. 

Ex:
if the grammar file is grammar.txt and the program file is prog.txt, run java grammar.txt prog.txt

OUTPUT
_______
Results will be printed to txt files and stdout.