import java.text.ParseException;

%%
%public
%class Lexer
%unicode
%line
%type TerminalType
%yylexthrow ParseException
%eofclose
%function nextToken

%{
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
	
public void writeTokens() throws IOException, ParseException {

		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				"tokens.out")));

		while (!zzAtEOF) {
			out.print(nextToken().toString());
		}

		yyreset(zzReader);
	}
%}


digit = [0-9]
number = {digit}+
letter = [a-zA-Z]
id = _({letter}|{digit})(({letter}|{digit}|_){0,8}) | {letter}({letter}|_|{digit}){0,9}
newline = \r|\n|\r\n|\u2028|\u2029|\u000B|\u000C|\u0085
whitespace = [ \t]+

%% 

"begin"		{ return TerminalType.BEGIN; }
"end"		{ return TerminalType.END; }
"print" 	{ return TerminalType.PRINT; }
"read"		{ return TerminalType.READ; }
":="		{ return TerminalType.ASSIGN; }
"("		{ return TerminalType.LEFTPAR; }
")"		{ return TerminalType.RIGHTPAR; }	
";"		{ return TerminalType.SEMICOLON; }
"+"		{ return TerminalType.PLUS; }
"-"		{ return TerminalType.MINUS; }
"*"		{ return TerminalType.MULTIPLY; }
"%"		{ return TerminalType.MODULO; }
","		{ return TerminalType.COMMA; }
{id}		{ return TerminalType.ID; }
{number}	{ return TerminalType.INTNUM; }
{newline}	{ yyline++; }
{whitespace}	{}
.		{ throw new ParseException("Illegal character <"+
                                                    yytext()+"> at " + yyline, yyline); }
